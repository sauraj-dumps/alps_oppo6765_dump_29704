function init () {
	addListenerToAllImgs();
}

function addListenerToAllImgs() {
	var objs = document.getElementsByTagName("img");
	for (var i = 0; i < objs.length; i++) {
		if (!objs[i].onclick) {
			objs[i].onclick = function() {
				BacaAndroid.onImgClickListener(this.src);
			}
		}
	}
}

function onImgClickListener (imgSrc) {
	BacaAndroid.onImgClickListener(imgSrc);
}

function changeTypeface (typeface) {
	document.body.style.fontFamily = typeface;
}

function showAndroidToast(toast) {
	BacaAndroid.showToast(toast);
}


function reload () {
	BacaAndroid.showToast("reload");
	window.location.reload( true );
}

function  setImgSrc(id,src){
	document.getElementById(id).src = src;
}


